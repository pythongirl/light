export default class CanvasManager {
    constructor(container) {
        this.container = container
        this.canvas = document.createElement("canvas")
        this.canvas.style.width = "100%"
        this.canvas.style.height = "100%"
        container.appendChild(this.canvas)

        if (typeof window.createREGL === "undefined") {
            throw new Error("REGL not found.")
        }

        this.regl = window.createREGL({
            canvas: this.canvas,
            extensions: ["OES_texture_float"]
        })
        this.renderScale = 1
        this.update()
    }

    update() {
        this.canvas.height = this.container.offsetHeight / this.renderScale
        this.canvas.width = this.container.offsetWidth / this.renderScale
        this.regl.poll()
    }
}